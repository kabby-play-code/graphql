package com.sample.graphql.resolvers;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.sample.graphql.model.Pet;
import com.sample.graphql.repository.PetRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Query implements GraphQLQueryResolver {

	private final PetRepository petRepository;

    public Iterable<Pet> pets() {
        return petRepository.findAll();
    }
    
    public Optional<Pet> petById(Long id) {
    	return petRepository.findById(id);
    }
    
    public List<Pet> petByName(String name) {
    	return petRepository.findByName(name);
    }
    
    public List<Pet>  petFindByNameOrAge(String name, int age){
    	return petRepository.findByNameOrAge(name, age);
    }

    
}