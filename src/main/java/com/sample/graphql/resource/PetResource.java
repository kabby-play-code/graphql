package com.sample.graphql.resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sample.graphql.model.Pet;
import com.sample.graphql.service.PetService;

@RestController
@RequestMapping(value="/pet")
public class PetResource {

	PetService petService;
	
	public PetResource(PetService petService) {
		this.petService=petService;
	}
	
	@PostMapping(value = { "/", "" })
	public Pet insertPet(@RequestBody Pet pet) {		
		return petService.savePet(pet);
	}
	
	
}
