package com.sample.graphql.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sample.graphql.model.Pet;
import com.sample.graphql.repository.PetRepository;

@Service
@Transactional
public class PetService {

	private final PetRepository petRepository;

	public PetService(PetRepository petRepository) {
		this.petRepository=petRepository;
	}
	
	public Pet savePet(Pet pet) {
		petRepository.save(pet);
		return pet;
	}
}
