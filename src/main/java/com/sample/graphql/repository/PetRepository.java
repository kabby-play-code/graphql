package com.sample.graphql.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sample.graphql.model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {
	
	List<Pet> findByName(String name);
	List<Pet> findByNameOrAge(String name, int age);
	
}