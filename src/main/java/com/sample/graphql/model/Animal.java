package com.sample.graphql.model;

public enum Animal {
    DOG,
    CAT,
    BADGER,
    MAMMOTH
}